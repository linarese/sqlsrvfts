use db_test;
GO

create table tb_test(pk numeric not null, text nvarchar(40));
GO

create unique index tb_test_ndx001 on tb_test (pk) ;
GO

insert into tb_test values (1,'fracturar');
insert into tb_test values (2,'rotura');
insert into tb_test values (3,'romper');
insert into tb_test values (4,'fragmentar');
insert into tb_test values (5,'quebrantar');
insert into tb_test values (6,'lesionar');
Go

create fulltext catalog  FullTextCatalog WITH ACCENT_SENSITIVITY = OFF as default;
GO

create fulltext index on tb_test 
    (text Language 3082 Statistical_Semantics) 
    key index tb_test_ndx001
    on FullTextCatalog
    With stoplist=system, change_tracking auto;
GO

ALTER FULLTEXT INDEX ON tb_test STOP POPULATION;
ALTER FULLTEXT INDEX ON tb_test START FULL POPULATION;

WAITFOR DELAY '00:01:00'

select * from tb_test where contains(text,'FORMSOF (THESAURUS, Traumatologo)');
GO


