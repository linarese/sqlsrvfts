# mssql-agent-fts-ha-tools
# Maintainers: Microsoft Corporation (LuisBosquez and twright-msft on GitHub)
# GitRepo: https://github.com/Microsoft/mssql-docker

# Base OS layer: Latest Ubuntu LTS + mssql-server-linux (SQL Server engine + tools)
FROM microsoft/mssql-server-linux

#Install curl since it is needed to get repo config
# Get official Microsoft repository configuration
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y curl p7zip-full && \
    curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    curl https://packages.microsoft.com/config/ubuntu/16.04/mssql-server-2017.list| tee /etc/apt/sources.list.d/mssql-server.list && \
    apt-get update

# Install optional packages.  Comment out the ones you don't need
# RUN apt-get install -y mssql-server-agent
# RUN apt-get install -y mssql-server-ha
RUN apt-get install -y mssql-server-fts

# Copy msi install for Spanish semantics db
COPY ./data/SemanticLanguageDatabase.msi    /opt/mssql-tools/share/resources/es_ES/
# Extract msi  
RUN 7z x -o/opt/mssql-tools/share/resources/es_ES/ /opt/mssql-tools/share/resources/es_ES/SemanticLanguageDatabase.msi 

# Set env for sqlserver start properly
ENV ACCEPT_EULA=Y
ENV SA_PASSWORD=CTest001
ENV MSSQL_PID=Developer
# Copy Thesaurus
COPY ./data/tsesn.xml  /var/opt/mssql/FTData/

# Copy sql script to create, activate and check semantcsbd
COPY ./sq01-config.sql  /tmp/
COPY ./sq02-stopw.sql   /tmp/
COPY ./sq03-check.sql   /tmp/

#RUN /opt/mssql/bin/sqlservr & \
#    ( sleep 30 && \
#      /opt/mssql-tools/bin/sqlcmd -U sa -P CTest001 -i /tmp/sq01-runall.sql -e -o /tmp/sq01-config.sql.out \
#    )
    
# Run SQL Server process
ENTRYPOINT [ "/opt/mssql/bin/sqlservr" ]