CREATE DATABASE semanticsdb  
            ON (     FILENAME = '/opt/mssql-tools/share/resources/es_ES/Externals_SemanticLanguageDatabase_64' )  
            LOG ON ( FILENAME = '/opt/mssql-tools/share/resources/es_ES/Externals_SemanticLanguageLog_64' )  
            FOR ATTACH;
GO

EXEC sp_fulltext_semantic_register_language_statistics_db @dbname = N'semanticsdb';  
GO  

drop database if exists db_test;
GO

create database db_test collate Modern_Spanish_CI_AI;
Go

use db_test;
go

exec sys.sp_fulltext_load_thesaurus_file 3082;
GO

SELECT * FROM sys.fulltext_languages
GO

SELECT * FROM sys.fulltext_semantic_languages;
GO

SELECT * FROM sys.fulltext_semantic_language_statistics_database;
GO

